import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'WarehouseProject';
   openNav() {
    document.getElementById("mySidenav").style.width = "215px";
    document.getElementById("main").style.paddingLeft = "215px";

  }
  
  /* Set the width of the side navigation to 0 */
   closeNav() {
    //var width = document.getElementById('mySidenav').offsetWidth; alert(width);

    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.padding = "0";

  }
}
