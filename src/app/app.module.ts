
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VoucherComponent } from './WebApp/voucher/voucher.component';
import { VoucherRequestComponent } from './WebApp/voucherRequest/voucherRequest.component';
import { HomeComponent } from './WebApp/home/home.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserComponent } from './WebApp/user/user.component';
import { AddUserComponent } from './WebApp/add-user/add-user.component';
import { ItemComponent } from './WebApp/item/item.component';
import { HijoComponent } from './WebApp/hijo/hijo.component';
import { FormsModule } from '@angular/forms';

@NgModule({
   declarations: [
      AppComponent,
      VoucherComponent,
      VoucherRequestComponent,
      HomeComponent,
      UserComponent,
      AddUserComponent,
      ItemComponent,
      HijoComponent

   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      FormsModule,
      BrowserAnimationsModule, // required animations module
      ToastrModule.forRoot() // ToastrModule added
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
