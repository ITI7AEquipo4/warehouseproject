export interface IVoucher {
        id:number;
        invoiceNumber:number;
        orderDate:Date;
        deliverDate:Date;
        orderUser:string;
        deliverUser:string;   
        status:string;
}
