import { DecimalPipe } from '@angular/common';

export interface IVoucherItem {
    id:number;
    voucherID:number;
    name:string;
    description:string;
    itemID:number;
    qty:number;
    price:number;
    subtotal:number;
}
