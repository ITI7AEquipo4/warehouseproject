import { Component, OnInit, ViewChild  } from '@angular/core';
import { IVoucher } from 'src/app/Model/voucher.interface';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.component.html',
  styleUrls: ['./voucher.component.css']
})
export class VoucherComponent implements OnInit {
  public voucher:IVoucher;
  public voucherList:IVoucher[] = [
    {
      id: 1,
    invoiceNumber: 23,
    orderDate: new Date(2019,11, 1,10,20,10),
    orderUser: "John Smith",
    deliverUser: "Judas Iscariote",
    deliverDate: null,
    status: 'Pendiente'
    },
    {
      id: 2,
      invoiceNumber: 24,
      orderDate: new Date(2019,11, 2,10,22,32),
      orderUser: "Ned Stark",
      deliverUser: "Sean Bean",
      deliverDate: null,
      status: 'Pendiente'
    },
    {

      id: 3,
      invoiceNumber: 25,
      orderDate: new Date(2019,11, 2,10,22,32),
      orderUser: "Emilia Clark",
      deliverUser: "Khaleesi",
      deliverDate: null,
      status: 'Entregado'
    },
    {
      id: 4,
      invoiceNumber: 26,
      orderDate: new Date(2019,11, 2,10,22,32),
      orderUser: "Catelyn Stark",
      deliverUser: "Michelle Fairley",
      deliverDate: null,
      status: 'Cancelado'
    }
  ];

  constructor(private toastr: ToastrService) { }  
  
  ngOnInit(): void {  

    }


}
