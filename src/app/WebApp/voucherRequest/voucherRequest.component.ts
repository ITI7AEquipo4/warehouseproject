import { Component, OnInit } from '@angular/core';
import { IVoucherItem } from 'src/app/Model/voucher-item.interface';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-voucherRequest',
  templateUrl: './voucherRequest.component.html',
  styleUrls: ['./voucherRequest.component.css']
})
export class VoucherRequestComponent implements OnInit {

  constructor() { }
  public voucherItem:IVoucherItem;
  public voucherItemList:IVoucherItem[] = [
    {
      id: 1,
      voucherID: 23,
      itemID: 2,
      name: 'CBALUM',
      description: 'Cuadro de Aluminio',
      qty: 1,
      price: 700.00,
      subtotal: 700.00
    },
    {
      id: 2,
      voucherID: 23,
      itemID: 2,
      name: 'LR20',
      description: 'Llanta Rodado 20',
      qty: 2,
      price: 250.00,
      subtotal: 500.00
    },
    {

      id: 3,
      voucherID: 23,
      itemID: 3,
      name: 'MANALUM',
      description: 'Manubrio de aluminio',
      qty: 1,
      price: 200.00,
      subtotal: 200.00
    },
    {
      id: 4,
      voucherID: 23,
      itemID: 4,
      name: 'DIAFI',
      description: 'Diablos de fierro',
      qty: 1,
      price: 50.00,
      subtotal: 200.00
    }
  ];
  ngOnInit() {
  }

  deleteRow(voucherItem:IVoucherItem  )
  {
    const index = this.voucherItemList.indexOf(voucherItem, 0);
    if (index > -1) 
    {
    this.voucherItemList.splice(index, 1);
    }
  }
}
