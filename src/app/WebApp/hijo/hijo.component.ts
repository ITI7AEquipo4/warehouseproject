import { Component, OnInit, Input,EventEmitter,Output } from '@angular/core';
import { NgModule } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  constructor() { }
@Input() txtMessage:string;
@Output() Yes = new EventEmitter<boolean>();

  ngOnInit() {
  }
OnClick()
{
this.Yes.emit(true);

}
}
