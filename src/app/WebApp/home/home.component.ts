import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  message:string = "";
  IsVisible:boolean = false;
  constructor() { }

  ngOnInit() {
  }
  OnClickShowMessage()
  {
    this.IsVisible = !this.IsVisible;
  }
  onModalYes(value:boolean)
  {
    this.IsVisible = !value; 
    this.message ="";
  }
}
