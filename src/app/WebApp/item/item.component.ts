import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators, MaxLengthValidator} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';




@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  FilterItem='';
  ItemForm : FormGroup;
  
  
  constructor (private toastr: ToastrService)
  {
    this.ItemForm = this.createFormsGroup();
  }
  
  
  
    OnResetForm(){
      
      this.ItemForm.reset();
  
    }
  
    OnSaveForm(){
      if(this.ItemForm.valid)
      {
        console.log(this.ItemForm.value);
        console.log("Save");
        this.ItemForm.reset();
       
      }
      else
      {
        console.log("no es valido");
      }
      
    }
    
    
    get id() {return this.ItemForm.get('id');}
    get photo() {return this.ItemForm.get('photo');}
    get description() {return this.ItemForm.get('description');}
    get stock() {return this.ItemForm.get('stock');}
    get minStock() {return this.ItemForm.get('minStock');}
    get maxStock() {return this.ItemForm.get('maxStock');}
    get marginDays() {return this.ItemForm.get('marginDays');}
    get priceUnits() {return this.ItemForm.get('priceUnits');}
  
  
    public itemList=[];
  
    ngOnInit() {
  
    }
  
    FillData(data:any)
    {
      this.itemList = data
    }
    

    createFormsGroup ()
    {
      return new FormGroup({
      id: new FormControl (''),
      photo:new FormControl ('',[Validators.required]),
      description:new FormControl ('',[Validators.required,Validators.minLength(1)]),
      stock :new FormControl ('',[Validators.required,Validators.minLength(1),  Validators.pattern('[0-9]+') ]),
      maxStock: new FormControl ('',[Validators.required]),
      minStock : new FormControl ('',[Validators.required]),
      marginDays: new FormControl ('',[Validators.required]),
      priceUnits:new FormControl ('',[Validators.required]),
      });
    }
  }
  