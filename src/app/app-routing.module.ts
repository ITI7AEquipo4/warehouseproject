import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { VoucherComponent } from './WebApp/voucher/voucher.component';
import { VoucherRequestComponent } from './WebApp/voucherRequest/voucherRequest.component';
import { HomeComponent } from './WebApp/home/home.component';
import { UserComponent } from './WebApp/user/user.component';
import { AddUserComponent } from './WebApp/add-user/add-user.component';
import { ItemComponent } from './WebApp/item/item.component';


const routes: Routes = [
  {path:'voucher', component: VoucherComponent},
  {path:'vrequest', component: VoucherRequestComponent},
  {path:'home', component: HomeComponent},
  {path:'user', component: UserComponent},
  {path:'adduser', component: AddUserComponent},
  {path:'item', component: ItemComponent},

  { path: '*', component: HomeComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
